-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2016 at 10:41 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `warehouse4`
--

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_barang`
--

CREATE TABLE `warehouse_barang` (
  `ID_BARANG` int(11) NOT NULL,
  `ID_KATEGORI` int(11) DEFAULT NULL,
  `HARGA` bigint(20) DEFAULT NULL,
  `STOK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_hak_akses`
--

CREATE TABLE `warehouse_hak_akses` (
  `ID_HAK` int(11) NOT NULL,
  `NAMA_JABATAN` char(255) DEFAULT NULL,
  `KETERANGAN` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_kategori`
--

CREATE TABLE `warehouse_kategori` (
  `ID_KATEGORI` int(11) NOT NULL,
  `NAMA_KATEGORI` char(255) DEFAULT NULL,
  `DESKRIPSI` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_pembelian_barang`
--

CREATE TABLE `warehouse_pembelian_barang` (
  `ID_PURCHASE` int(11) NOT NULL,
  `ID_BARANG` int(11) NOT NULL,
  `JUMLAH` int(11) DEFAULT NULL,
  `HARGA_BARANG` bigint(20) DEFAULT NULL,
  `HARGA_JUAL` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_penjualan_barang`
--

CREATE TABLE `warehouse_penjualan_barang` (
  `ID_SALES_ORDER` int(11) NOT NULL,
  `ID_BARANG` int(11) NOT NULL,
  `JUMLAH` int(11) DEFAULT NULL,
  `LABA` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_purchase_order`
--

CREATE TABLE `warehouse_purchase_order` (
  `ID_PURCHASE` int(11) NOT NULL,
  `ID_SUPPLIER` int(11) DEFAULT NULL,
  `ID_USER` int(11) NOT NULL,
  `TANGGAL` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_sales_order`
--

CREATE TABLE `warehouse_sales_order` (
  `ID_SALES_ORDER` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `TANGGAL` datetime DEFAULT NULL,
  `TOTAL_PENJUALAN` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_supplier`
--

CREATE TABLE `warehouse_supplier` (
  `ID_SUPPLIER` int(11) NOT NULL,
  `NAMA_SUPPLIER` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for TABLE `warehouse_users`
--

CREATE TABLE `warehouse_users` (
  `ID_USER` int(11) NOT NULL,
  `ID_HAK` int(11) DEFAULT NULL,
  `NIP` char(255) DEFAULT NULL,
  `NAMA_USER` char(255) DEFAULT NULL,
  `USERNAME` char(255) DEFAULT NULL,
  `PASSWORD` char(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for TABLE `warehouse_barang`
--
ALTER TABLE `warehouse_barang`
  ADD PRIMARY KEY (`ID_BARANG`),
  ADD KEY `FK_RELATIONSHIP_11` (`ID_KATEGORI`);

--
-- Indexes for TABLE `warehouse_hak_akses`
--
ALTER TABLE `warehouse_hak_akses`
  ADD PRIMARY KEY (`ID_HAK`);

--
-- Indexes for TABLE `warehouse_kategori`
--
ALTER TABLE `warehouse_kategori`
  ADD PRIMARY KEY (`ID_KATEGORI`);

--
-- Indexes for TABLE `warehouse_pembelian_barang`
--
ALTER TABLE `warehouse_pembelian_barang`
  ADD PRIMARY KEY (`ID_PURCHASE`,`ID_BARANG`),
  ADD KEY `FK_RELATIONSHIP_7` (`ID_BARANG`);

--
-- Indexes for TABLE `warehouse_penjualan_barang`
--
ALTER TABLE `warehouse_penjualan_barang`
  ADD PRIMARY KEY (`ID_SALES_ORDER`,`ID_BARANG`),
  ADD KEY `FK_RELATIONSHIP_10` (`ID_BARANG`);

--
-- Indexes for TABLE `warehouse_purchase_order`
--
ALTER TABLE `warehouse_purchase_order`
  ADD PRIMARY KEY (`ID_PURCHASE`),
  ADD KEY `FK_MEMBELI` (`ID_USER`),
  ADD KEY `FK_RELATIONSHIP_8` (`ID_SUPPLIER`);

--
-- Indexes for TABLE `warehouse_sales_order`
--
ALTER TABLE `warehouse_sales_order`
  ADD PRIMARY KEY (`ID_SALES_ORDER`),
  ADD KEY `FK_MENJUAL` (`ID_USER`);

--
-- Indexes for TABLE `warehouse_supplier`
--
ALTER TABLE `warehouse_supplier`
  ADD PRIMARY KEY (`ID_SUPPLIER`);

--
-- Indexes for TABLE `warehouse_users`
--
ALTER TABLE `warehouse_users`
  ADD PRIMARY KEY (`ID_USER`),
  ADD KEY `FK_MEMILIKI` (`ID_HAK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for TABLE `warehouse_barang`
--
ALTER TABLE `warehouse_barang`
  MODIFY `ID_BARANG` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_hak_akses`
--
ALTER TABLE `warehouse_hak_akses`
  MODIFY `ID_HAK` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_kategori`
--
ALTER TABLE `warehouse_kategori`
  MODIFY `ID_KATEGORI` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_penjualan_barang`
--
ALTER TABLE `warehouse_penjualan_barang`
  MODIFY `ID_SALES_ORDER` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_purchase_order`
--
ALTER TABLE `warehouse_purchase_order`
  MODIFY `ID_PURCHASE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_sales_order`
--
ALTER TABLE `warehouse_sales_order`
  MODIFY `ID_SALES_ORDER` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_supplier`
--
ALTER TABLE `warehouse_supplier`
  MODIFY `ID_SUPPLIER` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for TABLE `warehouse_users`
--
ALTER TABLE `warehouse_users`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for TABLE `warehouse_barang`
--
ALTER TABLE `warehouse_barang`
  ADD CONSTRAINT `FK_RELATIONSHIP_11` FOREIGN KEY (`ID_KATEGORI`) REFERENCES `warehouse_kategori` (`ID_KATEGORI`);

--
-- Constraints for TABLE `warehouse_pembelian_barang`
--
ALTER TABLE `warehouse_pembelian_barang`
  ADD CONSTRAINT `FK_RELATIONSHIP_6` FOREIGN KEY (`ID_PURCHASE`) REFERENCES `warehouse_purchase_order` (`ID_PURCHASE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_7` FOREIGN KEY (`ID_BARANG`) REFERENCES `warehouse_barang` (`ID_BARANG`);

--
-- Constraints for TABLE `warehouse_penjualan_barang`
--
ALTER TABLE `warehouse_penjualan_barang`
  ADD CONSTRAINT `FK_RELATIONSHIP_10` FOREIGN KEY (`ID_BARANG`) REFERENCES `warehouse_barang` (`ID_BARANG`),
  ADD CONSTRAINT `FK_RELATIONSHIP_9` FOREIGN KEY (`ID_SALES_ORDER`) REFERENCES `warehouse_sales_order` (`ID_SALES_ORDER`);

--
-- Constraints for TABLE `warehouse_purchase_order`
--
ALTER TABLE `warehouse_purchase_order`
  ADD CONSTRAINT `FK_MEMBELI` FOREIGN KEY (`ID_USER`) REFERENCES `warehouse_users` (`ID_USER`),
  ADD CONSTRAINT `FK_RELATIONSHIP_8` FOREIGN KEY (`ID_SUPPLIER`) REFERENCES `warehouse_supplier` (`ID_SUPPLIER`);

--
-- Constraints for TABLE `warehouse_sales_order`
--
ALTER TABLE `warehouse_sales_order`
  ADD CONSTRAINT `FK_MENJUAL` FOREIGN KEY (`ID_USER`) REFERENCES `warehouse_users` (`ID_USER`);

--
-- Constraints for TABLE `warehouse_users`
--
ALTER TABLE `warehouse_users`
  ADD CONSTRAINT `FK_MEMILIKI` FOREIGN KEY (`ID_HAK`) REFERENCES `warehouse_hak_akses` (`ID_HAK`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
