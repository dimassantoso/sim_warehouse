-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2016 at 01:04 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autocom`
--

-- --------------------------------------------------------

--
-- Table structure for table `bangunan`
--

CREATE TABLE `bangunan` (
  `id_bangunan` int(11) NOT NULL,
  `no_bangunan` int(11) NOT NULL,
  `luas` int(11) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `lokasi` longtext NOT NULL,
  `status` varchar(20) NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bangunan`
--

INSERT INTO `bangunan` (`id_bangunan`, `no_bangunan`, `luas`, `harga`, `lokasi`, `status`, `deskripsi`) VALUES
(1, 1001, 450, 256000000000, 'keputih wetan', 'kawin', 'luas dan tinggi'),
(2, 154, 500, 125000000, 'kejawen gebang', 'kosongan, strategis', 'berharga');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `hutang` int(11) NOT NULL,
  `sisa_hutang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cicilan_bank`
--

CREATE TABLE `cicilan_bank` (
  `id_cicilan` int(11) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `giro`
--

CREATE TABLE `giro` (
  `id_giro` int(11) NOT NULL,
  `kode_giro` varchar(32) NOT NULL,
  `pengirim` varchar(50) NOT NULL,
  `bank_asal` varchar(50) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `bank_tujuan` varchar(50) NOT NULL,
  `nominal` int(11) NOT NULL,
  `tgl_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`id`, `name`) VALUES
(1, 'insert'),
(2, 'update'),
(3, 'delete'),
(4, 'select');

-- --------------------------------------------------------

--
-- Table structure for table `hutang`
--

CREATE TABLE `hutang` (
  `id_hutang` int(11) NOT NULL,
  `nama_pemberi_pinjaman` varchar(50) NOT NULL,
  `jumlah_hutang` bigint(20) NOT NULL DEFAULT '0',
  `sisa_hutang` bigint(20) NOT NULL DEFAULT '0',
  `tanggal_hutang` date DEFAULT NULL,
  `jatuh_tempo` date NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hutang`
--

INSERT INTO `hutang` (`id_hutang`, `nama_pemberi_pinjaman`, `jumlah_hutang`, `sisa_hutang`, `tanggal_hutang`, `jatuh_tempo`, `deskripsi`) VALUES
(2, 'mamad', 15000, 0, '2016-01-11', '2016-01-27', 'hutang buat minum'),
(3, 'rama', 150000, 0, '2016-01-12', '2016-01-28', 'hutang baterai');

-- --------------------------------------------------------

--
-- Table structure for table `hutang_bank`
--

CREATE TABLE `hutang_bank` (
  `id_hutang_bank` int(11) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `tgl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jumlah` int(11) NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id_kas` int(11) NOT NULL,
  `tipe` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `deskripsi` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kas`
--

INSERT INTO `kas` (`id_kas`, `tipe`, `jumlah`, `tanggal`, `deskripsi`) VALUES
(2, 1, 25000, '2016-01-11', 'pemasukkan dari pajak'),
(3, 0, 50000, '2016-01-11', 'pemasukkan dari bapak'),
(4, 0, 60000, '2016-01-05', 'dimakan reno				                				                ');

-- --------------------------------------------------------

--
-- Table structure for table `mesin`
--

CREATE TABLE `mesin` (
  `id_mesin` int(11) NOT NULL,
  `no_mesin` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modal`
--

CREATE TABLE `modal` (
  `id_modal` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` longtext NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modal`
--

INSERT INTO `modal` (`id_modal`, `jumlah`, `keterangan`, `tanggal`) VALUES
(2, 1000000, 'modal perusahaan sendiri', '2016-01-11'),
(3, 550000, 'modalnya teman loo', '2016-01-05');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `name`) VALUES
(1, 'security'),
(2, 'accounting'),
(3, 'e-commerce'),
(4, 'warehouse'),
(5, 'human-resource'),
(6, 'payroll'),
(7, 'logistik'),
(8, 'surat-arsip');

-- --------------------------------------------------------

--
-- Table structure for table `piutang`
--

CREATE TABLE `piutang` (
  `id_piutang` int(11) NOT NULL,
  `nama_peminjam` varchar(50) NOT NULL,
  `jumlah_piutang` bigint(20) NOT NULL,
  `sisa_piutang` bigint(20) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `piutang`
--

INSERT INTO `piutang` (`id_piutang`, `nama_peminjam`, `jumlah_piutang`, `sisa_piutang`, `tanggal_pinjam`, `jatuh_tempo`, `deskripsi`) VALUES
(1, 'rama', 45000, 0, '2016-01-04', '2016-01-29', 'pinjam buat beli makan');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`) VALUES
(1, 'super user'),
(7, 'bos besar'),
(8, 'manager hrd bro'),
(9, 'admin_warehouse');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `id` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `hak_akses` int(11) NOT NULL,
  `module` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role`, `hak_akses`, `module`) VALUES
(81, 7, 1, 5),
(82, 7, 2, 5),
(83, 7, 1, 6),
(84, 7, 4, 6),
(85, 7, 1, 7),
(86, 7, 2, 7),
(87, 7, 4, 7),
(96, 8, 2, 1),
(97, 8, 4, 1),
(98, 8, 2, 2),
(99, 8, 4, 2),
(100, 1, 1, 1),
(101, 1, 2, 1),
(102, 1, 3, 1),
(103, 1, 4, 1),
(104, 1, 1, 2),
(105, 1, 2, 2),
(106, 1, 3, 2),
(107, 1, 4, 2),
(108, 1, 1, 3),
(109, 1, 2, 3),
(110, 1, 3, 3),
(111, 1, 4, 3),
(112, 1, 1, 4),
(113, 1, 2, 4),
(114, 1, 3, 4),
(115, 1, 4, 4),
(116, 1, 1, 5),
(117, 1, 2, 5),
(118, 1, 3, 5),
(119, 1, 4, 5),
(120, 1, 1, 6),
(121, 1, 2, 6),
(122, 1, 3, 6),
(123, 1, 4, 6),
(124, 1, 1, 7),
(125, 1, 2, 7),
(126, 1, 3, 7),
(127, 1, 4, 7),
(128, 1, 1, 8),
(129, 1, 2, 8),
(130, 1, 3, 8),
(131, 1, 4, 8),
(132, 9, 1, 4),
(133, 9, 2, 4),
(134, 9, 3, 4),
(135, 9, 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tanah`
--

CREATE TABLE `tanah` (
  `id_tanah` int(11) NOT NULL,
  `no_tanah` varchar(32) NOT NULL,
  `luas` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `lokasi` longtext NOT NULL,
  `status` varchar(20) NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanah`
--

INSERT INTO `tanah` (`id_tanah`, `no_tanah`, `luas`, `harga`, `lokasi`, `status`, `deskripsi`) VALUES
(1, '2001', 500, 256000000, 'keputih no 26', '-', '-'),
(2, '2002', 500, 1000000000, 'kutub utara', 'murah bro', 'datar');

-- --------------------------------------------------------

--
-- Table structure for table `transakasi_hp`
--

CREATE TABLE `transakasi_hp` (
  `id_transaksi` int(11) NOT NULL,
  `id_hutangpiutang` int(11) NOT NULL,
  `ket` int(1) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `nominal_dibayar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transakasi_hp`
--

INSERT INTO `transakasi_hp` (`id_transaksi`, `id_hutangpiutang`, `ket`, `tgl_transaksi`, `nominal_dibayar`) VALUES
(1, 2, 0, '2016-01-11', 5000),
(2, 2, 0, '2016-01-11', 2000),
(4, 1, 1, '2016-01-11', 5500);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `authKey` varchar(100) NOT NULL,
  `accessToken` varchar(100) NOT NULL,
  `user_type` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `sec_question` text NOT NULL,
  `sec_answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `authKey`, `accessToken`, `user_type`, `email`, `sec_question`, `sec_answer`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', 3, 'tiyo.lab@gmail.com', 'siapa tuhanmu?', 'Allah'),
(2, 'reno', '41ae5118f87c9f621ef5d66c698e0a94', '', '', 5, 'reno@reno', 'siapa tuhanmu', 'Allah'),
(10, 'ghozi', 'a89410e723dc2f6d0c67ee05e13cb604', '', '', 5, 'ghozi@gmail', 'nama project?', 'autocom2'),
(12, 'bima', '7fcba392d4dcca33791a44cd892b2112', '', '', 5, 'lalal@gmail.com', 'nama project?', 'autocom');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `name`, `role`) VALUES
(2, 'super user', 1),
(3, 'super admin', 1),
(5, 'manager 1 hrd', 8),
(6, 'ayam bakar', 8),
(7, 'supervisor', 9);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_user_role`
--
CREATE TABLE `v_user_role` (
`id_user` int(11)
,`id_role` int(11)
,`role` varchar(32)
,`user_type` varchar(32)
,`module` int(11)
,`hak_akses` text
);

-- --------------------------------------------------------

--
-- Table structure for table `w_barang`
--

CREATE TABLE `w_barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(20) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_kategori_barang`
--

CREATE TABLE `w_kategori_barang` (
  `id_kategori_barang` int(11) NOT NULL,
  `nama_kategori` varchar(256) NOT NULL,
  `deskripsi` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_pembelian_barang`
--

CREATE TABLE `w_pembelian_barang` (
  `jumlah` int(11) NOT NULL,
  `harga_barang` int(100) NOT NULL,
  `harga_jual` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_penjualan_barang`
--

CREATE TABLE `w_penjualan_barang` (
  `jumlah` int(11) NOT NULL,
  `laba` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_purchase_order`
--

CREATE TABLE `w_purchase_order` (
  `id_purchase` int(11) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_sales_order`
--

CREATE TABLE `w_sales_order` (
  `id_sales` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `total_penjualan` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_supplier`
--

CREATE TABLE `w_supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `w_users`
--

CREATE TABLE `w_users` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(255) NOT NULL,
  `unlocked` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `w_users`
--

INSERT INTO `w_users` (`id_user`, `nama_user`, `unlocked`) VALUES
(14, 'W Kepolo ', 0),
(15, 'Superrr Visor', 0),
(16, 'Si operator', 0),
(17, 'Si Warehouse Admin', 1),
(31, 'Admin baru', 1),
(32, 'ADmin 2', 1);

-- --------------------------------------------------------

--
-- Structure for view `v_user_role`
--
DROP TABLE IF EXISTS `v_user_role`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user_role`  AS  select `user`.`id` AS `id_user`,`role`.`id` AS `id_role`,`role`.`name` AS `role`,`user_type`.`name` AS `user_type`,`module`.`id` AS `module`,group_concat(`role_module`.`hak_akses` separator ',') AS `hak_akses` from ((((`user` join `user_type`) join `role`) join `role_module`) join `module`) where ((`user`.`user_type` = `user_type`.`id`) and (`user_type`.`role` = `role`.`id`) and (`role_module`.`role` = `role`.`id`) and (`role_module`.`module` = `module`.`id`)) group by `role_module`.`module`,`role`.`id`,`user`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bangunan`
--
ALTER TABLE `bangunan`
  ADD PRIMARY KEY (`id_bangunan`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `cicilan_bank`
--
ALTER TABLE `cicilan_bank`
  ADD PRIMARY KEY (`id_cicilan`);

--
-- Indexes for table `giro`
--
ALTER TABLE `giro`
  ADD PRIMARY KEY (`id_giro`);

--
-- Indexes for table `hak_akses`
--
ALTER TABLE `hak_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hutang`
--
ALTER TABLE `hutang`
  ADD PRIMARY KEY (`id_hutang`);

--
-- Indexes for table `hutang_bank`
--
ALTER TABLE `hutang_bank`
  ADD PRIMARY KEY (`id_hutang_bank`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id_kas`);

--
-- Indexes for table `mesin`
--
ALTER TABLE `mesin`
  ADD PRIMARY KEY (`id_mesin`);

--
-- Indexes for table `modal`
--
ALTER TABLE `modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `piutang`
--
ALTER TABLE `piutang`
  ADD PRIMARY KEY (`id_piutang`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_module_role` (`role`),
  ADD KEY `fk_role_module_hak_akses` (`hak_akses`),
  ADD KEY `fk_role_module_module` (`module`);

--
-- Indexes for table `tanah`
--
ALTER TABLE `tanah`
  ADD PRIMARY KEY (`id_tanah`);

--
-- Indexes for table `transakasi_hp`
--
ALTER TABLE `transakasi_hp`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_user_type` (`user_type`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type_role` (`role`);

--
-- Indexes for table `w_barang`
--
ALTER TABLE `w_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `w_kategori_barang`
--
ALTER TABLE `w_kategori_barang`
  ADD PRIMARY KEY (`id_kategori_barang`);

--
-- Indexes for table `w_pembelian_barang`
--
ALTER TABLE `w_pembelian_barang`
  ADD PRIMARY KEY (`jumlah`);

--
-- Indexes for table `w_penjualan_barang`
--
ALTER TABLE `w_penjualan_barang`
  ADD PRIMARY KEY (`jumlah`);

--
-- Indexes for table `w_purchase_order`
--
ALTER TABLE `w_purchase_order`
  ADD PRIMARY KEY (`id_purchase`);

--
-- Indexes for table `w_sales_order`
--
ALTER TABLE `w_sales_order`
  ADD PRIMARY KEY (`id_sales`,`total_penjualan`);

--
-- Indexes for table `w_supplier`
--
ALTER TABLE `w_supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `w_users`
--
ALTER TABLE `w_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bangunan`
--
ALTER TABLE `bangunan`
  MODIFY `id_bangunan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `hak_akses`
--
ALTER TABLE `hak_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hutang`
--
ALTER TABLE `hutang`
  MODIFY `id_hutang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id_kas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `modal`
--
ALTER TABLE `modal`
  MODIFY `id_modal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `piutang`
--
ALTER TABLE `piutang`
  MODIFY `id_piutang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `tanah`
--
ALTER TABLE `tanah`
  MODIFY `id_tanah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transakasi_hp`
--
ALTER TABLE `transakasi_hp`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `w_barang`
--
ALTER TABLE `w_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `w_kategori_barang`
--
ALTER TABLE `w_kategori_barang`
  MODIFY `id_kategori_barang` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `w_pembelian_barang`
--
ALTER TABLE `w_pembelian_barang`
  MODIFY `jumlah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `w_penjualan_barang`
--
ALTER TABLE `w_penjualan_barang`
  MODIFY `jumlah` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `w_purchase_order`
--
ALTER TABLE `w_purchase_order`
  MODIFY `id_purchase` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `w_sales_order`
--
ALTER TABLE `w_sales_order`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `w_supplier`
--
ALTER TABLE `w_supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `fk_role_module_hak_akses` FOREIGN KEY (`hak_akses`) REFERENCES `hak_akses` (`id`),
  ADD CONSTRAINT `fk_role_module_module` FOREIGN KEY (`module`) REFERENCES `module` (`id`),
  ADD CONSTRAINT `fk_role_module_role` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_user_type` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`id`);

--
-- Constraints for table `user_type`
--
ALTER TABLE `user_type`
  ADD CONSTRAINT `user_type_role` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
