<?php

use yii\helpers\Html;
use app\models\Modal;
use app\models\Kas;
use app\models\Tanah;
use app\models\Bangunan;
use app\models\Hutang;
use app\models\Piutang;
use yii\bootstrap\ActiveForm;



if(!empty(Yii::$app->request->post())){
	$data = Yii::$app->request->post();
	list($bulan_from, $tanggal_from, $tahun_from) = explode("/", $data['tanggal_from']);
	list($bulan_to, $tanggal_to, $tahun_to) = explode("/", $data['tanggal_to']);

	$kas = new Kas();
	$bangunan = new Bangunan();
	$tanah = new Tanah();
	$modal = new Modal();
	$hutang = new Hutang();
	$piutang = new Piutang();

	$resultKas = floatval($kas->getIncomeKasBetween( "$tahun_from-$bulan_from-$tanggal_from", "$tahun_to-$bulan_to-$tanggal_to")['income']) - floatval($kas->getOutcomeKasBetween("$tahun_from-$bulan_from-$tanggal_from", "$tahun_to-$bulan_to-$tanggal_to")['outcome']);
	$resultBangunan = floatval($bangunan->getTotalHargaBangunan()['total_harga']);
	$resultTanah = floatval($tanah->getTotalHargaTanah()['total_harga']);
	$resultModal = floatval($modal->getTotalModal()['total_modal']);
	$resultHutang = floatval($hutang->getKewajibanHutang( "$tahun_from-$bulan_from-$tanggal_from", "$tahun_to-$bulan_to-$tanggal_to")['hutang']);
	$resultPiutang = floatval($piutang->getHartaPiutang( "$tahun_from-$bulan_from-$tanggal_from", "$tahun_to-$bulan_to-$tanggal_to")['piutang']);

}


?>

<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>list of Kas</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<?php $form = ActiveForm::begin([
							'id' => 'create-role-form',
							'options' => ['class' => 'form-horizontal', 'data-toggle'=>'validator', 'role'=>'form'],
							'fieldConfig' => [
							'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
							'labelOptions' => ['class' => 'col-lg-1 control-label'],
							],
					]); ?>
						<div class="col-md-12">
							<div class="col-md-2">
								<div class="form-group">
						                <label>Date From</label>
						                <input type="text" placeholder="" class="form-control input-datepicker" name="tanggal_from" required = "required">
						        </div>	
							</div>
							<div class="col-md-2">
								<div class="form-group">
						                <label>Date Destination</label>
						                <input type="text" placeholder="" class="form-control input-datepicker" name="tanggal_to" required = "required">
						        </div>	
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label></label><br>
						           	<?= Html::submitButton('Show', ['class' => 'btn btn-primary']) ?>
						        </div>	
							</div>
						</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
	</div>
</div>

<?php
if(!empty(Yii::$app->request->post())){
?>

<div class="col-md-12">
    <div class="widget widget-green">
        <div class="widget-title">
            <div class="widget-controls">
				<a href="#" class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top" title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-full-screen widget-control-show-when-full" data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i class="fa fa-expand"></i></a>
				<a href="#" class="widget-control widget-control-refresh" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fa fa-refresh"></i></a>
				<a href="#" class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top" title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
			</div>
            <h3><i class="fa fa-ok-circle"></i>list of Kas</h3>
        </div>
			<div class="widget-content">
				<div class="row">
					<table>
						<tr>
							<td colspan="5" style="text-align:center"><u>Neraca PT. Autocom</u></td>
						</tr>
						<tr>
							<td colspan="5" style="text-align:center"><u>Per <?="$tahun_from-$bulan_from-$tanggal_from - $tahun_to-$bulan_to-$tanggal_to"?></u></td>
						</tr>
						<tr>
							<td>Harta</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i><u>Aktifitas lancar</u></i></b></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Kas</td>
							<td></td>
							<td>Rp.<?=$resultKas?></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Piutang</td>
							<td></td>
							<td>Rp.<?=$resultPiutang?></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i>Total aset lancar</i></b></td>
							<td></td>
							<td></td>
							<td>Rp.<?=($resultKas + $resultPiutang)?></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i><u>Aktifitas tetap</u></i></b></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Tanah</td>
							<td></td>
							<td>Rp.<?=$resultTanah?></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Bangunan</td>
							<td></td>
							<td>Rp.<?=$resultBangunan?></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i>Total aset lancar</i></b></td>
							<td></td>
							<td></td>
							<td>Rp.<?=($resultBangunan + $resultTanah)?></td>
						</tr>
						<tr>
							<td>Total harta</td>
							<td></td>
							<td></td>
							<td></td>
							<td>Rp.<?=($resultKas + $resultPiutang + $resultBangunan + $resultTanah)?></td>
						</tr>
						<tr>
							<td>Kewajiban dan ekuitas</td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i><u>Kewajiban</u></i></b></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Hutang</td>
							<td></td>
							<td>Rp.<?=$resultHutang?></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i>Total Kewajiban</i></b></td>
							<td></td>
							<td></td>
							<td>Rp.<?=($resultHutang)?></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i><u>Ekuitas</u></i></b></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>Modal</td>
							<td></td>
							<td>Rp.<?=$resultModal?></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td><b><i>Total Ekuitas</i></b></td>
							<td></td>
							<td></td>
							<td>Rp.<?=($resultModal)?></td>
						</tr>
						<tr>
							<td>Total kewajiban dan ekuitas</td>
							<td></td>
							<td></td>
							<td></td>
							<td>Rp.<?=($resultHutang + $resultModal)?></td>
						</tr>
					</table>
				</div>
			</div>
	</div>
</div>

<?php
}
?>