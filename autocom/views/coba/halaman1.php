<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Coba */
/* @var $form ActiveForm */
?>
<div class="coba-halaman1">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'nama') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- coba-halaman1 -->
