<?php

namespace app\controllers;

use Yii;
use app\controllers\SecureController;
use app\models\User;
use app\models\WUsers;

class WarehouseController extends SecureController{
	public $layout = "warehouse_layout";

	
	public function actionIndex(){
		$status = WUsers::findOne($_SESSION['__id']);
		$status = $status['unlocked'];
		if ($status==0) {
			return "Anda tidak mendapatkan hak akses<br>Hubungi Admin Warehouse";
			exit(0);
		}
		// print_r($_SESSION);
		return $this->render('index');
	}

	public function actionUsername()
	{
		$user = User::find()->where(['id'=>'17'])->asArray()->one();
		return $user['username'];
		return ;
	}
	public function actionCreate_organisasibarang(){
			return $this->render('create_organisasibarang');	
	}
	public function actionCreate_datasupplier(){
			return $this->render('create_datasupplier');	
	}
	public function actionUsers(){
			return $this->render('user_list');	
	}
	public function actionOrganisasibarang(){
		return $this->render('OrganisasiBarang');
	}
	public function actionDatasupplier(){
		return $this->render('DataSupplier');
	}
	public function actionPenjualan(){
		return $this->render('penjualan');
	}


	public function actionCreate_datapembelian(){
		return $this->render('create_datapembelian');
	}
	
	public function actionCreate_datapenjualan(){
		return $this->render('create_datapenjualan');
	}


	public function actionPembelian(){
		return $this->render('pembelian');
	}

	public function actionBarang(){
		return $this->render('barang');
	}

	public function actionKategoriBarang(){
		return $this->render('kategori_barang');
	}

	public function actionLaporan(){
		return $this->render('laporan');
	}


	public function actionCreateUser(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){
				$new_user = new User();
				$cek_uname = $new_user->findUsernameExists(Yii::$app->request->post()['username']);
				if ($cek_uname==null) {
					
					$new_user->saveBySQL(Yii::$app->request->post());
					$w_user = new WUsers();
					$w_user->saveBySQL(Yii::$app->request->post());

					return $this->redirect(['warehouse/users']);
				}else{
					print_r($cek_uname);
					return 'Username sudah terpakai';
				}

			}

        	return $this->render('create_user');
        }else{
        	echo "You don't have access here";die;	
        }
	}

		public function actionUpdateUser(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){
				$user = User::findOne(Yii::$app->request->get()['id']);
				$user->username = Yii::$app->request->post()['username'];
		        $user->email = Yii::$app->request->post()['email'];
		        $user->sec_question = Yii::$app->request->post()['sec_question'];
		        $user->sec_answer = Yii::$app->request->post()['sec_answer'];
		        $user->user_type = Yii::$app->request->post()['user_type'];
		        $user->authKey = "";
		        $user->accessToken = "";
		        $user->update();

				/*return $this->render('user_list');*/
				return $this->redirect(['warehouse/user-management']);
			}

        	return $this->render('update_user');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteUser(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				User::deleteAll('id = '.Yii::$app->request->get()['id']);

				/*return $this->render('user_list');*/
				return $this->redirect(['warehouse/user-management']);
			}

        	return $this->render('user_list');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdatelock($id_user, $status)
	{
		
			if(Yii::$app->request->get()){}
	}

}

?>