<?php

namespace app\controllers;
use app\models\Coba;

class CobaController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionHalaman1()
    {
    	$model = new Coba;
    	if ($model->load(Yii::app->request->post()) && $model->validate() ) {

    	}else
        return $this->render('halaman1', ['model'=>$model]);
    }

}
