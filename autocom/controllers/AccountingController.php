<?php

namespace app\controllers;

use Yii;
use app\controllers\SecureController;
use app\models\Modal;
use app\models\Kas;
use app\models\Tanah;
use app\models\Bangunan;
use app\models\Hutang;
use app\models\Piutang;
use app\models\TransaksiHp;

class AccountingController extends SecureController{

	public $layout = "accounting_layout";

	public function actionIndex(){
		return $this->render('index');
	}

	/**
	*
	* MODAL
	*/

	public function actionModalManagement(){
		if($this->isSelectAllowed()){
			return $this->render('list_modal');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddModal(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tanggal']);

				$modal = new Modal();
				$modal->jumlah = Yii::$app->request->post()['jumlah'];
				$modal->keterangan = Yii::$app->request->post()['keterangan'];
				$modal->tanggal = "$tahun-$bulan-$tanggal";
				$modal->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/modal-management']);
			}

        	return $this->render('add_modal');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateModal(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){
				
				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tanggal']);

				$modal = Modal::findOne(Yii::$app->request->get()['id']);
				$modal->jumlah = Yii::$app->request->post()['jumlah'];
				$modal->keterangan = Yii::$app->request->post()['keterangan'];
				$modal->tanggal = "$tahun-$bulan-$tanggal";
				$modal->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/modal-management']);
			}

        	return $this->render('update_modal');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteModal(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				Modal::deleteAll('id_modal = '.Yii::$app->request->get()['id']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/modal-management']);
			}

        	return $this->render('list_modal');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	/**
	*
	* KAS
	*/

	public function actionKasManagement(){
		if($this->isSelectAllowed()){
			return $this->render('list_kas');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddKas(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tanggal']);

				$kas = new Kas();
				$kas->tipe = Yii::$app->request->post()['tipe'];
				$kas->jumlah = Yii::$app->request->post()['jumlah'];
				$kas->deskripsi = Yii::$app->request->post()['deskripsi'];
				$kas->tanggal = "$tahun-$bulan-$tanggal";
				$kas->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/kas-management']);
			}

        	return $this->render('add_kas');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateKas(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){
				
				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tanggal']);

				$modal = Kas::findOne(Yii::$app->request->get()['id']);
				$modal->tipe = Yii::$app->request->post()['tipe'];
				$modal->jumlah = Yii::$app->request->post()['jumlah'];
				$modal->deskripsi = Yii::$app->request->post()['deskripsi'];
				$modal->tanggal = "$tahun-$bulan-$tanggal";
				$modal->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/kas-management']);
			}

        	return $this->render('update_kas');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteKas(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				Kas::deleteAll('id_kas = '.Yii::$app->request->get()['id']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/kas-management']);
			}

        	return $this->render('list_kas');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	/**
	*
	* TANAH
	*
	*/

	public function actionTanahManagement(){
		if($this->isSelectAllowed()){
			return $this->render('list_tanah');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddTanah(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				$tanah = new Tanah();
				$tanah->no_tanah = Yii::$app->request->post()['no_tanah'];
				$tanah->luas = Yii::$app->request->post()['luas'];
				$tanah->harga = Yii::$app->request->post()['harga'];
				$tanah->lokasi = Yii::$app->request->post()['lokasi'];
				$tanah->status = Yii::$app->request->post()['status'];
				$tanah->deskripsi = Yii::$app->request->post()['deskripsi'];
				$tanah->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/tanah-management']);
			}

        	return $this->render('add_tanah');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateTanah(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){

				$tanah = Tanah::findOne(Yii::$app->request->get()['id']);
				$tanah->no_tanah = Yii::$app->request->post()['no_tanah'];
				$tanah->luas = Yii::$app->request->post()['luas'];
				$tanah->harga = Yii::$app->request->post()['harga'];
				$tanah->lokasi = Yii::$app->request->post()['lokasi'];
				$tanah->status = Yii::$app->request->post()['status'];
				$tanah->deskripsi = Yii::$app->request->post()['deskripsi'];
				$tanah->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/tanah-management']);
			}

        	return $this->render('update_tanah');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteTanah(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				Tanah::deleteAll('id_tanah = '.Yii::$app->request->get()['id']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/tanah-management']);
			}

        	return $this->render('list_tanah');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	/**
	*
	* BANGUNAN
	*
	*/

	public function actionBangunanManagement(){
		if($this->isSelectAllowed()){
			return $this->render('list_bangunan');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddBangunan(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				$bangunan = new Bangunan();
				$bangunan->no_bangunan = Yii::$app->request->post()['no_bangunan'];
				$bangunan->luas = Yii::$app->request->post()['luas'];
				$bangunan->harga = Yii::$app->request->post()['harga'];
				$bangunan->lokasi = Yii::$app->request->post()['lokasi'];
				$bangunan->status = Yii::$app->request->post()['status'];
				$bangunan->deskripsi = Yii::$app->request->post()['deskripsi'];
				$bangunan->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/bangunan-management']);
			}

        	return $this->render('add_bangunan');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateBangunan(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){

				$tanah = Bangunan::findOne(Yii::$app->request->get()['id']);
				$tanah->no_bangunan = Yii::$app->request->post()['no_bangunan'];
				$tanah->luas = Yii::$app->request->post()['luas'];
				$tanah->harga = Yii::$app->request->post()['harga'];
				$tanah->lokasi = Yii::$app->request->post()['lokasi'];
				$tanah->status = Yii::$app->request->post()['status'];
				$tanah->deskripsi = Yii::$app->request->post()['deskripsi'];
				$tanah->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/bangunan-management']);
			}

        	return $this->render('update_bangunan');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteBangunan(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				Bangunan::deleteAll('id_bangunan = '.Yii::$app->request->get()['id']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/bangunan-management']);
			}

        	return $this->render('list_bangunan');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	/**
	*
	* NERACA
	*
	*/

	public function actionNeraca(){
		if($this->isSelectAllowed()){
			/*if(Yii::$app->request->post()){
				print_r(Yii::$app->request->post());die;
			}*/
			return $this->render('neraca');	
		}else{
			echo "You don't have access here";die;
		}
	}

	/**
	*
	* HUTANG
	*
	*/

	public function actionHutangManagement(){
		if($this->isSelectAllowed()){
			return $this->render('list_hutang');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddHutang(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				list($bulan_hutang, $tanggal_hutang, $tahun_hutang) = explode("/", Yii::$app->request->post()['tanggal_hutang']);
				list($bulan_tempo, $tanggal_tempo, $tahun_tempo) = explode("/", Yii::$app->request->post()['jatuh_tempo']);

				$hutang = new Hutang();
				$hutang->nama_pemberi_pinjaman = Yii::$app->request->post()['nama_pemberi_pinjaman'];
				$hutang->jumlah_hutang = Yii::$app->request->post()['jumlah_hutang'];
				$hutang->tanggal_hutang = "$tahun_hutang-$bulan_hutang-$tanggal_hutang";
				$hutang->jatuh_tempo = "$tahun_tempo-$bulan_tempo-$tanggal_tempo";
				$hutang->deskripsi = Yii::$app->request->post()['deskripsi'];
				$hutang->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/hutang-management']);
			}

        	return $this->render('add_hutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateHutang(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){

				list($bulan_hutang, $tanggal_hutang, $tahun_hutang) = explode("/", Yii::$app->request->post()['tanggal_hutang']);
				list($bulan_tempo, $tanggal_tempo, $tahun_tempo) = explode("/", Yii::$app->request->post()['jatuh_tempo']);

				$hutang = Hutang::findOne(Yii::$app->request->get()['id']);
				$hutang->nama_pemberi_pinjaman = Yii::$app->request->post()['nama_pemberi_pinjaman'];
				$hutang->jumlah_hutang = Yii::$app->request->post()['jumlah_hutang'];
				$hutang->tanggal_hutang = "$tahun_hutang-$bulan_hutang-$tanggal_hutang";
				$hutang->jatuh_tempo = "$tahun_tempo-$bulan_tempo-$tanggal_tempo";
				$hutang->deskripsi = Yii::$app->request->post()['deskripsi'];
				$hutang->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/hutang-management']);
			}

        	return $this->render('update_hutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteHutang(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				TransaksiHp::deleteAll(['id_hutangpiutang' => Yii::$app->request->get()['id'], 'ket'=>0]);
				Hutang::deleteAll('id_hutang = '.Yii::$app->request->get()['id']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/hutang-management']);
			}

        	return $this->render('list_hutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionCicilanHutang(){
		if($this->isSelectAllowed()){
			return $this->render('list_cicilan_hutang');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddCicilanHutang(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tgl_transaksi']);

				$hutang = new TransaksiHp();
				$hutang->id_hutangpiutang = Yii::$app->request->get()['id'];
				$hutang->ket = 0;
				$hutang->tgl_transaksi = "$tahun-$bulan-$tanggal";
				$hutang->nominal_dibayar = Yii::$app->request->post()['nominal_dibayar'];
				$hutang->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/cicilan-hutang','id'=>Yii::$app->request->get()['id']]);
			}

        	return $this->render('add_cicilan_hutang', ['id'=>Yii::$app->request->get()['id']]);
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateCicilanHutang(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){

				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tgl_transaksi']);

				$cicilan = TransaksiHp::findOne(Yii::$app->request->get()['id_cicilan']);
				$cicilan->id_hutangpiutang = Yii::$app->request->get()['id_hutang'];
				$cicilan->ket = 0;
				$cicilan->tgl_transaksi = "$tahun-$bulan-$tanggal";
				$cicilan->nominal_dibayar = Yii::$app->request->post()['nominal_dibayar'];
				$cicilan->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/cicilan-hutang','id'=>Yii::$app->request->get()['id_hutang']]);
			}

        	return $this->render('update_cicilan_hutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteCicilanHutang(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				TransaksiHp::deleteAll('id_transaksi = '.Yii::$app->request->get()['id_cicilan']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/cicilan-hutang','id'=>Yii::$app->request->get()['id_hutang']]);
			}

        	return $this->render('list_cicilan_hutang', ['id'=>Yii::$app->request->get()['id_hutang']]);
        }else{
        	echo "You don't have access here";die;	
        }
	}

	/**
	*
	* PIUTANG
	*
	*/

	public function actionPiutangManagement(){
		if($this->isSelectAllowed()){
			return $this->render('list_piutang');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddPiutang(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				list($bulan_hutang, $tanggal_hutang, $tahun_hutang) = explode("/", Yii::$app->request->post()['tanggal_pinjam']);
				list($bulan_tempo, $tanggal_tempo, $tahun_tempo) = explode("/", Yii::$app->request->post()['jatuh_tempo']);

				$hutang = new Piutang();
				$hutang->nama_peminjam = Yii::$app->request->post()['nama_peminjam'];
				$hutang->jumlah_piutang = Yii::$app->request->post()['jumlah_piutang'];
				$hutang->tanggal_pinjam = "$tahun_hutang-$bulan_hutang-$tanggal_hutang";
				$hutang->jatuh_tempo = "$tahun_tempo-$bulan_tempo-$tanggal_tempo";
				$hutang->deskripsi = Yii::$app->request->post()['deskripsi'];
				$hutang->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/piutang-management']);
			}

        	return $this->render('add_piutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdatePiutang(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){

				list($bulan_hutang, $tanggal_hutang, $tahun_hutang) = explode("/", Yii::$app->request->post()['tanggal_pinjam']);
				list($bulan_tempo, $tanggal_tempo, $tahun_tempo) = explode("/", Yii::$app->request->post()['jatuh_tempo']);

				$hutang = Piutang::findOne(Yii::$app->request->get()['id']);
				$hutang->nama_peminjam = Yii::$app->request->post()['nama_peminjam'];
				$hutang->jumlah_piutang = Yii::$app->request->post()['jumlah_piutang'];
				$hutang->tanggal_pinjam = "$tahun_hutang-$bulan_hutang-$tanggal_hutang";
				$hutang->jatuh_tempo = "$tahun_tempo-$bulan_tempo-$tanggal_tempo";
				$hutang->deskripsi = Yii::$app->request->post()['deskripsi'];
				$hutang->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/piutang-management']);
			}

        	return $this->render('update_piutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeletePiutang(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				TransaksiHp::deleteAll(['id_hutangpiutang' => Yii::$app->request->get()['id'], 'ket'=>1]);
				Piutang::deleteAll('id_piutang = '.Yii::$app->request->get()['id']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/piutang-management']);
			}

        	return $this->render('list_piutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionCicilanPiutang(){
		if($this->isSelectAllowed()){
			return $this->render('list_cicilan_piutang');	
		}else{
			echo "You don't have access here";die;
		}
	}

	public function actionAddCicilanPiutang(){
		if($this->isInsertAllowed()){
			if(Yii::$app->request->post()){

				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tgl_transaksi']);

				$hutang = new TransaksiHp();
				$hutang->id_hutangpiutang = Yii::$app->request->get()['id'];
				$hutang->ket = 1;
				$hutang->tgl_transaksi = "$tahun-$bulan-$tanggal";
				$hutang->nominal_dibayar = Yii::$app->request->post()['nominal_dibayar'];
				$hutang->save();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/cicilan-piutang','id'=>Yii::$app->request->get()['id']]);
			}

        	return $this->render('add_cicilan_piutang', ['id'=>Yii::$app->request->get()['id']]);
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionUpdateCicilanPiutang(){
		if($this->isUpdateAllowed()){
			if(Yii::$app->request->post()){

				list($bulan, $tanggal, $tahun) = explode("/", Yii::$app->request->post()['tgl_transaksi']);

				$cicilan = TransaksiHp::findOne(Yii::$app->request->get()['id_cicilan']);
				$cicilan->id_hutangpiutang = Yii::$app->request->get()['id_piutang'];
				$cicilan->ket = 1;
				$cicilan->tgl_transaksi = "$tahun-$bulan-$tanggal";
				$cicilan->nominal_dibayar = Yii::$app->request->post()['nominal_dibayar'];
				$cicilan->update();
				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/cicilan-piutang','id'=>Yii::$app->request->get()['id_piutang']]);
			}

        	return $this->render('update_cicilan_piutang');
        }else{
        	echo "You don't have access here";die;	
        }
	}

	public function actionDeleteCicilanPiutang(){
		if($this->isDeleteAllowed()){
			if(Yii::$app->request->get()){
				TransaksiHp::deleteAll('id_transaksi = '.Yii::$app->request->get()['id_cicilan']);

				/*return $this->render('role_list');*/
				return $this->redirect(['accounting/cicilan-piutang','id'=>Yii::$app->request->get()['id_piutang']]);
			}

        	return $this->render('list_cicilan_piutang', ['id'=>Yii::$app->request->get()['id_piutang']]);
        }else{
        	echo "You don't have access here";die;	
        }
	}
}

?>