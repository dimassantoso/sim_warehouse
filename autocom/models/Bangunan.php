<?php

namespace app\models;

use Yii;

class Bangunan extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'bangunan';
    }

    public function getTotalHargaBangunan(){
    	$sql = "select sum(harga) as total_harga from bangunan";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;
    }
}
?>