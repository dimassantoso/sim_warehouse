<?php

namespace app\models;

use Yii;

class Modal extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'modal';
    }

    public function getTotalModal(){
    	$sql = "select sum(jumlah) as total_modal from modal";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;
    }
}
?>