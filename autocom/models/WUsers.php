<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\UserType;
/**
 * This is the model class for table "{{%warehouse_penjualan_barang}}".
 *
 * @property integer $ID_SALES_ORDER
 * @property integer $ID_BARANG
 * @property integer $JUMLAH
 * @property string $LABA
 *
 * @property WarehouseBarang $iDBARANG
 * @property WarehouseSalesOrder $iDSALESORDER
 */
class WUsers extends User
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'w_users';
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Nama',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'user_type' => 'User Type',
            'email' => 'Email',
            'sec_question' => 'Sec Question',
            'sec_answer' => 'Sec Answer',
        ];
    }


    
    public function findUser(){
        $sql = "SELECT user.username, {$this->tableName()}.nama_user, {$this->tableName()}.unlocked ,user.id, user.email, user_type.id as id_user_type, user_type.name as user_type_name FROM ".$this->tableName().", user, user_type WHERE user_type.id = user.user_type AND user.id={$this->tableName()}.id_user";
        $model = self::findBySql($sql)->asArray()->all();
        return $model;
    }

    public function saveBySQL($data){
        // $sql = "select user.username, user.id, user.email, user_type.id as id_user_type, user_type.name as user_type_name from ".$this->tableName().", user_type where user_type.id = user.user_type";
        $user = User::find()->where(['username'=>$data['username']])->asArray()->one();

        $this->id_user = $user['id'];
        $this->nama_user = $data['name'];

        $tipe = UserType::find()->where(['id'=>$data['user_type']])->asArray()->one();
        $role = $tipe['role'];

        // $this->unlocked = ($tipe==9)?1 : 1;
        $this->save();
    }
}
