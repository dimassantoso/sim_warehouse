<?php

namespace app\models;

use Yii;

class Tanah extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'tanah';
    }

    public function getTotalHargaTanah(){
    	$sql = "select sum(harga) as total_harga from tanah";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;
    }
}
?>