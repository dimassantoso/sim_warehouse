<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coba".
 *
 * @property integer $id
 * @property string $nama
 */
class Coba extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coba';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @inheritdoc
     * @return CobaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CobaQuery(get_called_class());
    }
}
