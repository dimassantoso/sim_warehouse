<?php

namespace app\models;

use Yii;

class TransaksiHp extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'transakasi_hp';
    }

    public function getTotalCicilanHutang($id_hutang){
    	$sql = "select sum(nominal_dibayar) as dibayar from ".$this->tableName()." where ket = 0 group by id_hutangpiutang having id_hutangpiutang = $id_hutang";
        $model = self::findBySql($sql)->asArray()->one();
        return $model;
    }

    public function getTotalCicilanPiutang($id_piutang){
    	$sql = "select sum(nominal_dibayar) as dibayar from ".$this->tableName()." where ket = 1 group by id_hutangpiutang having id_hutangpiutang = $id_piutang";
        $model = self::findBySql($sql)->asArray()->one();
        return $model;
    }
}
?>