<?php

namespace app\models;

use Yii;

class Kas extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'kas';
    }

    public function getIncomeKasBetween($from, $to){
    	$sql = "SELECT sum(jumlah) as income from kas where tanggal between '$from' and '$to' group by tipe having tipe = 1";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;
    }

    public function getOutcomeKasBetween($from, $to){
    	$sql = "SELECT sum(jumlah) as outcome from kas where tanggal between '$from' and '$to' group by tipe having tipe = 0";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;
    }
}
?>