<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Coba]].
 *
 * @see Coba
 */
class CobaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Coba[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Coba|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}