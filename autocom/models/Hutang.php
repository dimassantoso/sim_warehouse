<?php

namespace app\models;

use Yii;

class Hutang extends \yii\db\ActiveRecord{
	public static function tableName()
    {
        return 'hutang';
    }

    public function getKewajibanHutang($from, $to){
    	$sql = 
    	"select sum(jumlah_hutang - coalesce(
		    (select sum(nominal_dibayar) as bayar 
		     from transakasi_hp 
		     where tgl_transaksi between '$from' and '$to'
		     group by id_hutangpiutang having id_hutangpiutang = id_hutang), 
		    0)) as hutang 
		from hutang 
		where tanggal_hutang between '$from' and '$to'";
    	$model = self::findBySql($sql)->asArray()->one();
        return $model;	
    }
}
?>